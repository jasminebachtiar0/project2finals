from django.shortcuts import render
from django.http import HttpResponse
from .models import Note

# Create your views here.
def index(request):
    if request.method == 'POST':
        new_note = Note()
        new_note.title = request.POST["title"]
        new_note.text = request.POST["text"]
        new_note.save()

    notes = Note.objects.all()
    context = {'notes': notes}

    return render(request, 'index.html', context)

